## Closed! This has been implemented as part of the arenaclient [project](https://gitlab.com/aiarena/aiarena-client)

## SC2 Match Runner GUI

# Installation   
   
1. Clone the [arenaclient repo](https://gitlab.com/aiarena/aiarena-client) and install it using `python setup.py install`.

2. Clone this repository in a different location.
   
3. Navigate to the repository.

4. Make sure you have node.js with npm installed <https://nodejs.org/>

5. Install Electron via `npm install electron`.

6. Install the rest of the dependancies using `npm install`. 
   
7. Start the gui using `npm start` or `electron .`.  


 
