'use strict';

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const dialog = electron.dialog

// This method will be called when Electron has finished
// initialization and is ready to create browser mainWindow.
// Some APIs can only be used after this event occurs.
var rq = require('request-promise');
var mainWindow = null;
var mainAddr = 'http://127.0.0.1:8080/';
let loading = null 


// spawn server and call the child process
var child = require('child_process').spawn('python', ['server.py'],{
    detached: true, 
    stdio: 'ignore',
    cwd: 'resources/flask_server'
  }, function(err, data) {
    if(err){
      console.log(err);
      return;
    }
    console.log(data.toString());
  });


function createWindow(){


    // Create the browser mainWindow
    mainWindow = new BrowserWindow({
      minWidth: 1920,
      minHeight: 1080,
      show: false,
      webPreferences: {
        nodeIntegration: true
      }
    });
  
    // Load the index page of the flask in local server
    mainWindow.loadURL(mainAddr);
    mainWindow.webContents.executeJavaScript(`
    var path = require('path');
    module.paths.push(path.resolve('node_modules'));
    module.paths.push(path.resolve('../node_modules'));
    module.paths.push(path.resolve(__dirname, '..', '..', 'electron', 'node_modules'));
    module.paths.push(path.resolve(__dirname, '..', '..', 'app', 'node_modules'));
    path = undefined;
  `);
  
    // ready the window with load url and show
    mainWindow.once('ready-to-show', () => {
      mainWindow.show();
      loading.hide();
      loading.close();
    });

  
    // Quit app when close
    mainWindow.on('closed', function(){
      mainWindow = null;
      // kill the server on exit
      child.unref();
    });
    mainWindow.webContents.openDevTools();
  };
  exports.selectDirectory = function () {
    const filepath = dialog.showOpenDialog(mainWindow, {
      properties: ['openDirectory']
    })
    return filepath;
  };
  
  var startUp = function(loading){
      console.log('1');
   
    rq(mainAddr)
      .then(function(htmlString){
        console.log('server started!');
        createWindow();

      })
      .catch(function(err){
        console.log('waiting for the server start...');
        startUp();
      });
  };

  app.on('ready', ()=>{
    loading = new BrowserWindow({show:false,frame:false,width: 800,
        height: 600});
    loading.loadURL('file://'+__dirname+'/loading.html') 
    console.log(__dirname) 
    loading.show()
    startUp()
    
})
  
  app.on('quit', function() {
      // kill the python on exit
      child.kill('SIGKILL')
  });
  
  app.on('window-all-closed', () => {
      // quit app if windows are closed
      if (process.platform !== 'darwin'){
          app.quit();
      }
  });